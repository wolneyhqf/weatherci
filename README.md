# Observações

- Foi utilizado AngularJS v.1.6 como Framework RIA

- Gerenciamento de dependências client-side com Bower

- Arquivos minificados com GruntJS

- Para executar o projeto, basta colocar o diretório /dist com os arquivos do projeto gerados pelo Grunt, em um servidor Apache ou que suporte o php.

- Os códigos fontes e recursos do projeto se encontram no diretório /app

- A aplicação pode ser acessada também pelo link http://174.138.37.22/weatherci