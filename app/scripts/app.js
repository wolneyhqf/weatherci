'use strict';

/**
 * @ngdoc overview
 * @name app
 * @description
 * # Configurações da aplicação
 */

// Variável para guardar valores globais da aplicação.
var MyApplication = [];
MyApplication.config = [];
// URL base da API openweathermap.org
MyApplication.config.weatherAPI = "http://api.openweathermap.org/data/2.5/";
// Chave de acesso para a API openweathermap.org
MyApplication.config.APPID = "00e457eb9c2fbb47a7c7ec4abb816622";

var app = angular.module("app", ["ngRoute"]);

// Configurando as rotas do single page application, para caso haja necessidade de adicionar
// novas páginas na aplicação.
app.config(['$routeProvider', function($routeProvider) {
    $routeProvider
    .when('/', {
        templateUrl: function(param) {
            return 'views/home.html' + '?random=' + Math.random();
        }
    })
    .otherwise({
        redirectTo: '/'
    })
}]);


// Configurando filtro para requisições HTTP.
app.config(['$httpProvider', function($httpProvider) {
    $httpProvider.interceptors.push([
        'PinesNotificationsService',
        'ProgressLoaderService',
        '$q', function ($PinesNotificationsService, ProgressLoaderService, $cookieStore, $q) {
        return {
            // Exibir alerta caso ocorra um erro interno na API do openweathermap.org.
            responseError: function(error) {
                ProgressLoaderService.end();
                if (error.status >= 500) {
                    console.log(error.data);
                    PinesNotificationsService.notify({
                        title: 'Error.',
                        text: 'An internal error occurred.',
                        type: 'error'
                    });
                }
                return $q.reject(error);
            }
        }
    }]);
}]);