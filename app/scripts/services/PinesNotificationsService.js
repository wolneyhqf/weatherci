'use strict';
/**
 * @ngdoc function
 * @name app.service:PinesNotificationsService
 * @description
 * # Service para função de notificações.
 */
app.factory('PinesNotificationsService', [ function() {
        return {
            notify : function(notifyObject) {
                var myStack = {"dir1":"down", "dir2":"right", "push":"top"};
                new PNotify({
                    title: notifyObject.title,
                    text: notifyObject.text,
                    addclass: "stack-custom",
                    type: notifyObject.type,
                    stack: myStack
                })
            }
        };
} ]);