'use strict';
 /**
 * @ngdoc function
 * @name app.service:ProgressLoaderService
 * @description
 * # Service para funções de iniciar e finalizar efeito visual de carregamento.
 */
app.factory('ProgressLoaderService', [ function() {
    return {
        start : function() {
            $('#progress').css('display','block');
            $('#progress-window').css('display','block');
        },
        set : function(progressLoaderPosition) {
            $('#progress').find('#progress-bar').css('width',progressLoaderPosition + '%');
        },
        end : function() {
            $('#progress').find('#progress-bar').css('width','100%');
            setTimeout(function(){ 
                $('#progress').css('display','none');
                $('#progress-window').css('display','none');
            }, 700);
        }
    };
} ]);