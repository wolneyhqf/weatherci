'use strict';

 /**
 * @ngdoc function
 * @name app.service:ProgressLoaderService
 * @description
 * # Service para funções de acesso a API do openweathermap.org
 */
app.factory('WeatherService',
	[
		'$http',
		function ($http) {
			return {
				getCitiesByIds: function (ids) {
					// https://openweathermap.org/current#severalid
					return $http.get(MyApplication.config.weatherAPI+'group?units=metric&APPID='+MyApplication.config.APPID+'&id='+ids);
				},
				getCitiesByName: function (name) {
					// https://openweathermap.org/current#cities
					return $http.get(MyApplication.config.weatherAPI+'find?units=metric&&APPID='+MyApplication.config.APPID+'&q='+name);
				},
				getCityById: function (id) {
					// https://openweathermap.org/current#cityid
					return $http.get(MyApplication.config.weatherAPI+'weather?units=metric&APPID='+MyApplication.config.APPID+'&id='+id);
				},
			};
		}
	]
);