'use strict';
/**
 * @ngdoc controller
 * @name app.controller:HomeController
 * @description
 * # Controlador com funções da tela/página home.html e seus modais.
 */
app.controller('HomeController', [
    'WeatherService',
    'ProgressLoaderService',
    function (WeatherService, ProgressLoaderService) {

        var self = this;

        // Ids das cidades padrão, carregadas com a aplicação
        self.defaultCitiesIds = "2885800,3394023,3390760,2648579,5809844";
        // Lista de cidades da tela home
        self.citiesWeatherList = [];
        // Lista de busca de cidades da tela home / modal newCityWeatherModal.html
        self.citiesWeatherListSearch = [];
        // Variável para o campo de busca de cidades da tela home / modal newCityWeatherModal.html
        self.newCityWeatherSearch = "";

        // Carrega as cidades padrão da tela home
        self.loadDefaultCities = function(){
            // Inicia efeito visual de carregamento
            ProgressLoaderService.start();
            ProgressLoaderService.set(50);
            WeatherService.getCitiesByIds(self.defaultCitiesIds).then(
                function successCallback(response){
                    self.citiesWeatherList = response.data.list;
                    // Encerra efeito visual de carregamento
                    ProgressLoaderService.end();
                },
                function errorCallback(response){
                    // Na promisse retornada, o tratamento de erro apenas encerra o efeito
                    // visual de carregamento, caso a API retorne erro interno >= 500,
                    // O filtro instalado no appConfig.js mostra uma mensagem para o usuário.
                    ProgressLoaderService.end();
                }
            );
        };

        // Remove a cidade pelo indice atual do elemento no array
        self.removeCityWeather = function(index){
            self.citiesWeatherList.splice(index, 1);
        };

        // Abre o modal newCityWeatherModal.html da tela home para busca por novas cidades
        self.openModalNewCityWeather = function(){
            self.citiesWeatherListSearch = [];
            self.newCityWeatherSearch = "";
            $("#newCityWeatherModal").modal("show");
        };

        // Submete busca por novas cidades no modal newCityWeatherModal.html
        self.submitSearch = function(){
            ProgressLoaderService.start();
            ProgressLoaderService.set(50);
            WeatherService.getCitiesByName(self.newCityWeatherSearch).then(
                function successCallback(response){
                    self.citiesWeatherListSearch = response.data.list;
                    ProgressLoaderService.end();
                },
                function errorCallback(response){
                    ProgressLoaderService.end();
                }
            );
        }

        // Adiciona cidade pesquisada no newCityWeatherModal.html
        self.addCityWeather = function(cityWeather, index){
            // Remove da lista de pesquisa
            self.citiesWeatherListSearch.splice(index, 1);
            // Adiciona na lista principal
            self.citiesWeatherList.push(cityWeather);
            // Caso não tenha mais cidades para adicionar, fecha o modal newCityWeatherModal.html
            if(self.citiesWeatherListSearch.length == 0){
                $("#newCityWeatherModal").modal("hide");
            }
        }

        self.loadDefaultCities();
    }
]);