module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
            },
            dist: {
                // the files to concatenate
                src: [
                    'app/scripts/app.js',
                    'app/scripts/services/**/*.js',
                    'app/scripts/controllers/**/*.js',
                ],
                // the location of the resulting JS file
                dest: 'dist/main.js'
            }
        },
        htmlmin: {
            dist: {
                options: {
                    removeComments: true,
                    collapseWhitespace: true
                },
                files: [{
                    'dist/index.php' : 'app/index.php'
                },{
                    expand: true,
                    cwd: 'app/views',
                    src: '**/*.html',
                    dest: 'dist/views',
                }]
            }
        },
        cssmin: {
            target: {
                files: {
                    'dist/main.css': ['app/css/main.css']
                }
            }
        },
        ngdocs: {
            all: {
                src: ['app/scripts/**/*.js'],
                title: 'Development Docs'
            }
        },
        watch: {
            files: [
                'app/scripts/controllers/**/*.js',
                'app/scripts/services/**/*.js',
                'app/**/*.js',
                'app/**/*.css',
                'app/views/**/*.html',
                'app/**/*.php'
            ],
            tasks: ['uglify','htmlmin','cssmin']
        }
    });

    // Load the plugin that provides the "uglify" task.
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-htmlmin');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-ngdocs');

    grunt.registerTask('default', ['uglify','htmlmin', 'cssmin', 'ngdocs']);
    grunt.registerTask('w', ['watch']);

};